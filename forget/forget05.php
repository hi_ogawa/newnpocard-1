<?php		// forget05.php

 //
	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
//	mb_http_output('UTF-8');//	session_set_cookie_params(0, "/", "/members/", TRUE, TRUE);		
	session_start();
/*	
	if (!isset($_SESSION['auth_forget'])||($_SESSION['auth_forget'] != hash("sha512", $magic_code))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}	
*/
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
 <script src="../javascript/jquery-1.10.2.js"></script>
<script src="../javascript/jquery-corner.js"></script>
<script>
	jQuery(function() {
		$("#confirm").corner() 
	});
</script>
<style type="text/css">
	#main { width:1000px; margin-left:auto; margin-right:auto;background-color:black;}
	h1 {color:white; text-align:center;font-size:42px;}
	td {font-size:26px;font-weight:bold;}
	td.title {width:300px; text-align:left; color:white; background-color:red;}
	td.v {width 650px; text-align:left; padding-left:5px; color:red; background-color:white;}
	input {width:500px; font-size:26px; font-color:brown; font-weight:bold; background-color:yellow; border:2px solid brown; padding:3px;}
	#confirm {width:200px; margin-top:20px;font-size:36px; border: 4px solid brown; padding: 3px; background-color:yellow; color:red; text-align:center; margin-left:auto; margin-right:auto;}
</style>
	<title>NPO TRI</title>
</head>
<body>
<div id="main">
<h1><br>Reset Your Password</h1>

<table>
<form action="forget06.php" method="post">
	<input type="hidden" name="dr_tbl_id" value="<?= $_SESSION['dr_tbl_id'] ?>">
	
    <tr><td class="title">Your fullname in your language : </td><td class="v"><?= _Q($_SESSION['dr_name']);?></td></tr>
    <tr><td class="title">Your alphabetical name : </td><td class="v"><?= _Q($_SESSION['sirname'])." "._Q($_SESSION['firstname']); ?></td></tr>     
	<tr><td class="title">Affiliation : </td><td class="v"><?= _Q($_SESSION['hp_name']); ?></td></tr>
	<tr><td class="title">Email address : </td><td class="v"><?= _Q($_SESSION['email']); ?></td></tr>

	<tr><td class="title">New password : </td><td><input class="pw" type="password" id="dr_pwd" name="dr_pwd" size=30 maxlength=32></td></tr>
	<tr><td class="title">Retype new password : </td><td><input class="pw" type="password" id="dr_pwd2" name="dr_pwd2" size=30 maxlength=32></td></tr>

<tr><td colspan="2" align="center" ><input type="submit" value="- Confirm -" id="confirm"></td></tr>
</form>
</table>
</div>
</body>
</html>