<?php
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");	
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}
	if (!auth_dr()) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}	
	//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}
/*	
	$stmt = $pdo->prepare("SELECT `role_tbl`.`conf_tbl_id`, `role_tbl`.`dr_tbl_id`, `role_tbl`.`role_kind`, ".
	"`role_tbl`.`topic_title`, `role_tbl`.`topic_abstract`, `conf_tbl`.`id`, `conf_tbl`.`begin`, `conf_tbl`.`end`, ".
	"`conf_tbl`.`conf_jname`, `conf_tbl`.`conf_ename`, `conf_tbl`.`jplace`, `conf_tbl`.`eplace`, `conf_tbl`.`man_size`, ".
	"`dr_tbl`.`id`, `dr_tbl`.`dr_name`, `dr_tbl`.`dr_name_alpha`, `dr_tbl`.`is_male`, `dr_tbl`.`hp_name`, `dr_tbl`.`job_kind`, ".
	"`dr_tbl`.`birth_year`, `dr_tbl`.`email`, `dr_tbl`.`is_active`, `dr_tbl`.`is_usable` ".
	"FROM `role_tbl` INNER JOIN `conf_tbl` ON `role_tbl`.`conf_tbl_id` = `conf_tbl`.`id` ".
		"INNER JOIN `dr_tbl` ON `role_tbl`.`dr_tbl_id` = `dr_tbl`.`id` WHERE  `role_tbl`.`id` = :role_tbl_id;");
*/
	$stmt = $pdo->prepare("SELECT * FROM `role_tbl` WHERE `id` = :role_tbl_id;");
	$stmt->bindValue(":role_tbl_id", $_POST['role_tbl_id']);
	$stmt->execute();
	$row_role_tbl = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$stmt = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `id` = :conf_tbl_id;");
	$stmt->bindValue(":conf_tbl_id", $row_role_tbl['conf_tbl_id']);
	$stmt->execute();
	$row_conf_tbl = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `id` = :dr_tbl_id;");
	$stmt->bindValue(":dr_tbl_id", $row_role_tbl['dr_tbl_id']);
	$stmt->execute();
	$row_dr_tbl = $stmt->fetch(PDO::FETCH_ASSOC);

?>	
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--
/*********************************************************
	2014/07/15変更（はじめ）
*********************************************************/
<script type="text/javascript" src="../../ajax/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../../ajax/jquery-corner.js"></script>
-->
<script type="text/javascript" src="../../javascript/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../../javascript/jquery-corner.js"></script>
<!--
/*********************************************************
	2014/07/15変更（おわり）
*********************************************************/
-->

<script type="text/javascript" src="topic_view.js"></script>
<script type="text/javascript">
	function modify(number) {
		var frm = "frm"+number;
		document.getElementById(frm).submit();
	}
</script>
<link rel="stylesheet" type="text/css" href="topic_css.css"/>
<title>NPO TRI International Network</title>
</head>
<body>

<div id="main">
<!--
/*********************************************************
	2014/07/15変更（はじめ）
*********************************************************/
<p class="welcome">Welcome Mr/Ms <?= _Q($row_dr_tbl['dr_name_alpha']); ?>　　</p>
-->
<p class="welcome">Welcome Mr/Ms <?= _Q($row_dr_tbl['sirname']); ?>　　</p>
<!--
/*********************************************************
	2014/07/15変更（おわり）
*********************************************************/
-->


<h1>Your Presentation Contribution</h1>
<h2>This is your presentation during <?= $row_conf_tbl['conf_ename'] ?>.<br />
これは <?= $row_conf_tbl['conf_jname'] ?>におけるあなたの演題登録です</h2>
<?php
/*
	$stmt->bindValue(":role_tbl_id", $_POST['role_tbl_id']);
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);	

	foreach ($rows as $value) {
*/
?>	
<form action="view02.php" method="post" id="frm<?= _Q($_POST['role_tbl_id']) ?>">
	<input type="hidden" name="dr_tbl_id" value="<?= _Q($row_dr_tbl['id']) ?>" />
    <input type="hidden" name="conf_ename" value="<?= _Q($row_conf_tbl['conf_ename']) ?>" />
    <input type="hidden" name="conf_jname" value="<?= _Q($row_conf_tbl['conf_jname']) ?>" />
<!--
/*********************************************************
	2014/07/15変更（はじめ）
*********************************************************/
    <input type="hidden" name="conf_tbl_id" value="<?= _Q($conf_conf_tbl['id']) ?>" />
-->
    <input type="hidden" name="conf_tbl_id" value="<?= _Q($row_conf_tbl['id']) ?>" />
<!--
/*********************************************************
	2014/07/15変更（おわり）
*********************************************************/
-->
    <input type="hidden" name="dr_name" value="<?= _Q($row_dr_tbl['dr_name']) ?>" />
<!--
/*********************************************************
	2014/07/15変更（はじめ）
*********************************************************/
    <input type="hidden" name="dr_name_alpha" value="<?= _Q($row_dr_tbl['dr_name_alpha']) ?>" />
-->
    <input type="hidden" name="sirname" value="<?= _Q($row_dr_tbl['sirname']) ?>" />
<!--
/*********************************************************
	2014/07/15変更（おわり）
*********************************************************/
-->
    <input type="hidden" name="role_kind" value="<?= _Q($row_role_tbl['role_kind']) ?>" />
    <input type="hidden" name="role_tbl_id" value="<?= _Q($_POST['role_tbl_id']) ?>" />
    <table>	   
	<tr><td class="title">Your Role</td>
    <td class="title">
    <?php
		echo $role_kinds[$row_role_tbl['role_kind']];

	?>
    </td></tr>
	<tr><td class="title">Presentation Title :<br /><span class="alert">Up to 128 characters<br />日本語では64文字まで</span></td>
	<td><textarea name="topic_title" rows="3" cols="60"><?= _Q($row_role_tbl['topic_title']); ?></textarea></td></tr>
	<tr><td class="title">Presentation Abstract :<br /><span class="alert">Up to 2,000 characters<br />日本語では1,000文字まで</span></td>
	<td><textarea name="topic_abstract" rows="18" cols="60"><?=  _Q($row_role_tbl['topic_abstract']); ?></textarea></td></tr></table> 
    </form>
<br />
<div id="responce"><button class="confirm" onClick="modify(<?= _Q($_POST['role_tbl_id']) ?>)">Modify (修正する)</button>　　　　　　　　<button id="cancel">OK/Cancel (戻る)</button></div>
<br /><br />
<?php
/*
	}
*/
?>
</div>
</body>
</html>
