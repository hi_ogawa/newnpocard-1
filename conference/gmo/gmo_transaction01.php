﻿<?php
	//GMO_transaction01.php

	//**************************************************************
	//	GMOサイトより決済完了後の注文情報等更新および結果表示
	//	
	//**************************************************************

	//設定ファイルの読み込み
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");
	require_once("order_dao.php");
	require_once("gmo_mail.php");

	//追加設定ファイルの読み込み
	require_once("../../utilities/ext_conf.php");

	charSetUTF8();
	session_start();


	//初期化

	$DateTime=date('YmdHis'); //日時情報 yyyyMMddhhmmss書式 例 20130514142305 mycartより受け取るように変更

   	$Amount=0;
	$Tax=0;

/*
	//リロード対策
	if (!isset($_SESSION['ticket'])) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied----------</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
*/

	//DB更新
//	unset($_SESSION['ticket']);

	$OrderID=$_POST["OrderID"];
	$AccessID=$_POST["AccessID"];
	$TranDate=$_POST["TranDate"];
	$ErrCode=$_POST["ErrCode"];
	$ErrInfo=$_POST["ErrInfo"];
	$TranID=$_POST["TranID"];

	
	if ($ErrCode!="")
	{
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'>エラー</font></h1>";
		echo "<h2 align='center'>".$ErrCode."</h1>";
		echo "<h2 align='center'>".$ErrInfo."</h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

	$arr=explode("-",$OrderID);

	//$arr[1]：dr_tbl_id
	if (!array_key_exists(1, $arr))
	{
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

	//$arr[2]：conf_tbl_id
	if (!array_key_exists(2, $arr))
	{
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

 	try
	{
	
		// DBへの登録
		$orderDao=new order_dao($OrderID,$arr[1],$arr[2]);

		$orderDao->insert($AccessID);
		$orderDao->close();

		//決済完了PDFファイル作成
		//メール送信

	}
	catch(Exception $e)
	{
    		die($e->getMessage());
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

?>

<!-- これ以降に払込完了通知をメールで行い、ユーザーにも知らせる	-->
<?php

/*
Array (
[forget_flag] => 1
[email] => test1_20140729_15443@win-int.co.jp
[dr_name] => test_dr_name1_201407
[sirname] => testSirname
[firstname] => testFirstname
[dr_name_alpha] => testSirname testFirstname
[birth_year] => 1970-01-01
[is_male] => 1
[dr_tbl_id] => 486
[job_kind] => 1
[hp_name] => test_hp_name
[auth_dr_code] =>
2f31c7eb2682d2746569593ed4ad3f4db013a4635f178e32d86c0563a44c38fb3d075a6ebbca
cc840e0fd96c1950732daece1b54c66e1fb3adca28dc4c539365
[last_time] => 1406620765
[index_key] =>
250a64a8529dc6c389224690ded4b924c48ef780f33515b8bc36d79dba57853bd5ca58b90c05
44b43f7a647717a322b0dc02e96c9451e9af87af6bac833c1eab
[can_go_payment01] => 1
[can_go_gmo_transaction] => 1
)
*/

		// メール送信
		$mail=new gmo_mail();
		
		try
		{
			$mail->send($_SESSION['dr_name'],$_SESSION['email'],$_SESSION['hp_name'],$_SESSION['job_kind']);


			$error="";
			$error=error_get_last();
			$errorMsg="";


			if($error!="")
			{
				$mail->send2admin($_SESSION['dr_name'],$_SESSION['email'],$_SESSION['hp_name'],$_SESSION['job_kind']);
				$errorMsg="<br>・・・・・ご登録のメールアドレス「".$_SESSION['email']."」にメールが送信できませんでした。<br>";
				$errorMsg=$errorMsg."大変申し訳ありませんが、以下のメールアドレスにメール送信エラー、氏名等を記載いただき、メール送信をお願いします。<br>";
				$errorMsg=$errorMsg.$support_mail;

			}
		}
		catch(Exception $e)
		{
			echo "--------------";
		}


		$_SESSION = array();
		session_destroy();
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
	<script src="../../javascript/jquery-1.10.2.js"></script>
    <link rel="stylesheet" type="text/css" href="../../mem_reg/member_reg_mail.css">
	<title>事前登録完了通知</title>
</head>
<body>
と
<h1><br/>事前登録完了通知<br/></h1>
<div align="center">
<table width="80%">
<tr>
  <td class="r">
あなたの事前登録と払込が完了しました。恐れ入りますが、あなたのメールをご確認して下さい。
メーラーの設定によっては、迷惑メールに振り分けられる場合がありますので、迷惑メール・フォルダーも
ご確認下さい。
  <?=$errorMsg?></td></tr>
</table>
</div>
</body>
</html>
