<?php
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");	
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		$_SESSION = array();
		header('Location: ../../index.php');
	}

	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
	}
//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `conf_tbl` WHERE `id` = :conf_tbl_id;");
	$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$registerable = $row['man_size'];
	$stmt1 = $pdo->prepare("SELECT * FROM `conf_link_tbl` WHERE `conf_tbl_id` = :conf_tbl_id;");
	$stmt1->bindValue(":conf_tbl_id", $conf_tbl_id);
	$stmt1->execute();
	$registered = $stmt1->rowCount();
?>	
	
	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">


<title>NPO Registration</title>
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.css">
    
    <!-- Custom styles for this template -->
	<style type="text/css">
		.container {width: 90%; background-color:skyblue; height:90%;}
		.center {margin-left:auto; margin-right:auto;text-align:center;}
	</style>
</head>

<body>
<div class="space"><p></p></div>
<div class="container">
<div class="row">
<div class="col-lg-12">
<h1 class="alert-danger"><?= _Q($row['conf_jname']) ?>申し込み</h1>
<h1 class="alert-success">Pre-registration for <?= _Q($row['conf_ename']) ?></h1>
<h3 class="alert-info">残り登録可能人数 = <?= $registerable - $registered ?> 人です</h3>
<h3 class="alert-info">We can accept <?= $registerable - $registered ?> more participants.</h3>
<br><br>

<?php
	if (!auth_dr()) {
?>
<h3>Login is needed! (ログインが必要です!)</h3>
<h3><a href="../../index.php">登録が未だの方はこちらに</a></h3>
<h3><a href="../../index.php">If you have not yet retistered yourself in NPO TRI International Netowrk -></a></h3>
<?php
	} else {
?>
<form action="meeting_member02.php" method="post" id="goto02">
<input type="hidden" name="dr_tbl_id" value="<?= _Q($_POST['dr_tbl_id']) ?>">
<input type="hidden" name="conf_tbl_id" value="<?= _Q($conf_tbl_id) ?>" />

<div class="center"><button class="btn btn-large btn-primary" type="button" id="ok">&hearts; 了解 (OK) &hearts;</button></div>
</form>
<br><br>
</div>
</div>
</div>
<?php
	}
?>
    <!-- Bootstrap core javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../../javascript/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
	<script src="../../javascript/jquery-corner.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	jQuery(function() {
		 $("#ok").click(function() {
			 $("#goto02").submit();
		 });
	});
	</script>
</body>
</html>