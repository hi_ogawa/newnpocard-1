<?php
	require_once("../../utilities/config.php");
	require_once("../../utilities/lib.php");
	charSetUTF8();
	session_start();
	$conf_tbl_id = $_POST['conf_tbl_id'];

	if (!auth_dr()) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

	if (!isset($_POST['dr_tbl_id'])||!is_numeric($_POST['dr_tbl_id'])||!isset($_POST['conf_tbl_id'])||!is_numeric($_POST['conf_tbl_id'])) {
		echo "<body bgcolor='red'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	} else {
		$conf_tbl_id = $_POST['conf_tbl_id'];
		$dr_tbl_id = $_POST['dr_tbl_id'];
	}
//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `conf_link_tbl` WHERE `conf_tbl_id` = :conf_tbl_id AND `dr_tbl_id` = :dr_tbl_id;");
	$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
	$stmt->bindValue(":dr_tbl_id", $dr_tbl_id);
	$stmt->execute();
	if ($stmt->rowCount() > 0) {
		header('Location: ../../index.php');
		exit();
	} else {

		$stmt = $pdo->prepare("INSERT INTO `conf_link_tbl` (`conf_tbl_id`, `dr_tbl_id`) VALUES (:conf_tbl_id, :dr_tbl_id);");
		$stmt->bindValue(":conf_tbl_id", $conf_tbl_id);
		$stmt->bindValue(":dr_tbl_id", $dr_tbl_id);
		$stmt->execute();
		$conf_link_tbl_id= $pdo->lastInsertId();
?>

<?php
				$subject = $_SESSION['conf_jname'];
				$sender = mb_encode_mimeheader("特定非営利活動法人ティー・アール・アイ国際ネットワーク");
				$headers  = "FROM: ".$sender."<$support_mail>\r\n";		

				$parameters = '-f'.$support_mail;
	
	//　ここからメール内容
	$body = <<< _EOT_
	{$_SESSION['dr_name']} 様
	Dear Dr/Mr/Ms {$_SESSION['dr_name']},
	
	この度は {$_SESSION['conf_jname']}へ事前登録ありがとうございます。
	再度ログインして頂けると、事前割引カード決済や、演題登録が可能となります
	ログイン・アドレス {$site_url}/index.php
	
	Thank you for your application to {$_SESSION['conf_ename']}.
	After login the system again, you can pay by credit card with discount or
	submit your abstract.
	Login address: {$site_url}/index.php
	
	Best regards,
	
	Shigeru SAITO, MD, FACC, FSCAI, FJCC
	===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	
		if ($_SERVER['SERVER_NAME'] === 'localhost') echo $body;
		mb_language('uni');
		mb_internal_encoding('utf-8');
		mb_send_mail($_SESSION['email'], $subject, $body, $headers, "-f$support_mail"); 
?>

	
<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">

<title>NPO Registration</title>
    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.css">
    
    <!-- Custom styles for this template -->
	<style type="text/css">
		td {text-align:left; padding-left:3px;}
		.container {width: 90%; background-color:skyblue; height:90%;}
		.center {margin-left:auto; margin-right:auto;text-align:center;}
		h1 {color:red;}
	</style>
</head>

<body>
<div class="space"><p></p></div>
<div class="container">
<h1 class="center"><?= $_SESSION['conf_jname'] ?>参加登録が受け付けられました</h1>
<h1 class="center">あなた様にメールを発送しておりますので、ご確認お願いします</h1>
<br><br>
<h1 class="center">Your application to <?= $_SESSION['conf_jname'] ?> was accepted.</h1>
<h1 class="center">We sent you an email. Could you watch it?</h1>
<br><br>
<form action="../../index.php" method="POST" id="goto_next">
	<input type="hidden" name="dr_tbl_id" value="<?= $dr_tbl_id ?>">
    <input type="hidden" name="conf_tbl_id" value="<?= $conf_tbl_id ?>">
    <input type="hidden" name="conf_link_tbl_id" value="<?= $conf_linK_tbl_id ?>">
<table class="center">
<tr><td align="center"><br><br><button class="btn btn-large btn-primary" type="button" id="ok">- I understand (了解) -</button></td></tr>
</table>
</form>
</div>

    <!-- Bootstrap core javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> -->
<?php
	if ($_SERVER['HTTP_HOST'] == 'localhost') {
		echo '<script src="../../javascript/jquery-1.10.2.js"></script>';
	} else {
		echo '<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>';
	}
?>
	<script src="../../javascript/jquery-corner.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	jQuery(function() {
		 $("#ok").click(function() {
			 $("#goto_next").submit();
		 });
	});
	</script>
</body>
</html>

<?php
		$_SESSION = array();
		session_destroy();
		exit();
	}
?>

	