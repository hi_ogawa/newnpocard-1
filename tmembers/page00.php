﻿<!DOCTYPE html>
<html lang="ja">
	<head>
	<meta charset="utf-8">

        <SCRIPT type="text/javascript">
        <!--
            /*************************************
             *
             *  送信前のチェックを実施
             *
             *************************************/
            function isChecked()
            {
		//チェックが必要な場合、以下に記載
                return true;
            }
            /*************************************
             *
             *  ログイン
             *
             *************************************/
            function login()
            {
                var checkResult=false;         
				checkResult=isChecked();
                if(checkResult==true)
                {
                    document.forms["dataForm"].method="post";
                    /*document.forms["dataForm"].action="./login.php"; */
					document.forms["dataForm"].action="login.php";
                    document.forms["dataForm"].submit();
                }
               
            }
            /*************************************
             *
             *  新規登録
             *
             *************************************/
            function register()
            {
                var checkResult=false;
				checkResult=isChecked();
                if(checkResult==true)
                {
                    document.forms["dataForm"].method="post";
                    /*document.forms["dataForm"].action="./registerUser.php"; */
					 document.forms["dataForm"].action="registerUser.php";
                    document.forms["dataForm"].submit();
                }
            }
    
        -->
        </SCRIPT>

    </HEAD>
    <BODY>
        <H1>【テスト】年会費のお支払い手続き画面</H1>
        
        <form NAME="dataForm">
            <input type="hidden" NAME="LoginID" id="LoginID" value="">

            <TABLE border=1>
                <TR>
                    <TH>区分</TH>
                    <TH>金額(税込)</TH>
                </TR>
                <TR>
                    <TD>Dr</TD>
                    <TD>3000</TD>
                </TR>
                <TR>
                    <TD>コメディカル</TD>
                    <TD>5000</TD>
                </TR>
                <TR>
                    <TD>ノンメディカル</TD>
                    <TD>10000</TD>
                </TR>
            </TABLE>

	すでにログイン名をお持ちの方は以下よりお願いします。
            <TABLE border=1>
                <TR>
                    <TD>メールアドレス（ログイン名）</TD>
                    <TD><input type="text" id="loginID" NAME="loginID"></TD>
                </TR>
                <TR>
                    <TD>パスワード</TD>
                    <TD><input type="password" id="password" NAME="password"></TD>
                </TR>
                    <TD>&nbsp</TD>
                    <TD><input type="button" value="申込" onclick="login()"></TD>
                </TR>
            </TABLE>
	<br>
	<br>
	<br>
	初めての方は以下よりお願いします。
            <TABLE border=1>
                <TR>
                    <TD>メールアドレス（ログイン名）</TD>
                    <TD><input type="text" id="loginID2DB" NAME="loginID2DB"></TD>
                </TR>
                <TR>
                    <TD>職種</TD>
                    <TD>
			<SELECT NAME="job_kind2DB">
				<OPTION VALUE="DR">Dr</OPTION>
				<OPTION VALUE="コメディカル">コメディカル</OPTION>
				<OPTION VALUE="ノンメディカル">ノンメディカル</OPTION>
			</SELECT>
		　　</TD>
                </TR>
                    <TD>&nbsp</TD>
                    <TD><input type="button" value="申込" onclick="register()"></TD>
                </TR>
            </TABLE>

        </form>      
    </BODY>
</HTML>
