<?php  //auth_login02.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');
	charSetUTF8();
	session_start();

	if (!isset($_SESSION['index_key'])||($_SESSION['index_key'] != hash("sha512", $magic_code))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
	if (!isset($_SESSION['auth_admin'])||($_SESSION['auth_admin'] != hash("sha512", $magic_code.'facc'))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
 //
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../conference/meetings/meeting_member.css"/>
<title>管理者</title>
</head>

<body>
<h1>管理者ページ</h1>
<div id="main">

<form action="../admin/conf_add01.php" method="post">
	<input type="submit" class="submit" value="- カンファレンス追加 -" />
</form>
<h1><br /><br /><br /><br /></h1>
<form action="initialize.php" method="post">
	<input type="submit" id="submit" value="-　データベースの初期化 -" />
</form>
</div>   
</body>
</html>