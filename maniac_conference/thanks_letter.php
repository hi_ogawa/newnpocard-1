<?php		// thanks_letter.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');
	session_start();
	ini_set("mbstring.internal_encoding","UTF-8"); //このプログラムがUTF-8で書かれていることを宣言。
//	mb_language("japanese");
	mb_internal_encoding("UTF-8");	
	mb_http_output("UTF-8");
	mb_language("uni"); //Unicode（UTf-8）でメール送信するための宣言。この宣言により、mb_send_mail()関数利用時に、下記の赤字のヘッダーが自動付与され送信されます。
	//Content-Type: text/plain; charset=UTF-8
	//Content-Transfer-Encoding: BASE64←「本文もBase64エンコードしています」という宣言。
	//サブジェクトが長い場合は自動的に分割してくれます。	


	// DB接続
 	try {
    // MySQLサーバへ接続
   		$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}	
	
	// 今日の日付を取得
	$dt = new DateTime();
	$dt->setTimeZone(new DateTimeZone('Asia/Tokyo'));
	$current_time = $dt->format('Y-m-d H:i:s');
 
	// 比較した日付と時間を設定
	$target_time = '2013-07-01 00:00:01';
 
	// 日付を比較
	if (strtotime($target_time) >= strtotime($current_time)) {
//    	echo '指定した日付は未来です。';
		$job_kind = 90;	// 開催日過ぎれば参加資格あるものにのみ送付する
	} else {
//    	echo '指定した日付は過去です。';
		$job_kind = 91;
	}


	// 登録者のメルアドと名前取得
	$stmt = $pdo->prepare("SELECT `dr_name`, `dr_name_alpha`, `hp_name`, `email` FROM `dr_tbl` WHERE `job_kind` <= :job_kind;");
	$stmt->bindValue(":job_kind", $job_kind);
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$count = 0;
	
//// 必要書類の送付	
	$today = date("Y-F-d H:i:s");

	//文書の取得と文書のエンコード
//	$doc1 = file_get_contents("NAUSCIA_AMI_consent.doc");
//	$doc_encode64_001 = chunk_split(base64_encode($doc1));
//	$doc2 = file_get_contents("Live_Cases.pdf");
//	$doc_encode64_002 = chunk_split(base64_encode($doc2));	
//	$doc3 = file_get_contents("Final_Program_2nd_Maniac.pdf");
//	$doc_encode64_003 = chunk_split(base64_encode($doc3));
//	$doc4 = file_get_contents("Venue_Guidance_in_JAPANESE.pdf");
//	$doc_encode64_004 = chunk_split(base64_encode($doc4));		
//	$doc5 = file_get_contents("presentation_uploaded.pdf");
//	$doc_encode64_005 = chunk_split(base64_encode($doc5));
//	$doc6 = file_get_contents("Room_LUMIERE_and_Auditorium.pdf");
//	$doc_encode64_006 = chunk_split(base64_encode($doc8));
	$doc7 = file_get_contents("Thanks_Letter.pdf");
	$doc_encode64_007 = chunk_split(base64_encode($doc7));						
foreach($rows as $row){	
	$count += 1;
	//ヘッダ情報
	$sendto   = $row['email'];
//	$sendto = 'transradial@kamakuraheart.org';
	$subject  = "Thanks Letter from the 2nd Shonan TRI Maniac COnference";
	$sender = mb_encode_mimeheader("2nd Shonan TRI Maniac Conference");
	$headers  = "FROM: ".$sender."<fightingradialist@tri-international.org>\r\n";	
	$headers .= 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-Type: multipart/related;boundary="1000000000"' . "\r\n";

$message =<<<END
--1000000000
Content-Type: text/plain; charset=iso-2022-jp
Content-Transfer-Encoding: 7bit

Dear Dr. {$row['dr_name_alpha']};

Thank you for your participation in the 2nd Shonan TRI
Maniac Conference while sparing your busy schedule.
The meeting was very successful and interactive. 
I owe all of this success to your active participation.
I wish we would see you in a few years again in this
beautiful area.

Thanks again,

Sincerely yours,

Shigeru SAITO. 

{$row['dr_name']}　様

お忙しい中第二回湘南TRIマニアック・カンファランスにご参加下さり、
誠にありがとうございました。このカンファランスは大成功に終了しました。
この成功は全て皆様の活発なご参加の故と考えております。
再び数年以内にこの美しい場所で再開できますことを希望します。
ありがとうございました。
宜しくお願いします。

齋藤　滋

--1000000000
Content-Type: application/pdf; name="Thanks Letter.pdf"
Content-Transfer-Encoding: base64
Content-ID: <doc_id_07>

$doc_encode64_007

END;



//メール用にサブジェクトと本文をエンコード
//mb_internal_encoding('utf-8');
$subject = mb_encode_mimeheader($subject);
$message = mb_convert_encoding($message, "JIS");
//$message = mb_convert_encoding($message, 'UTF-8');	// これだと日本語文字化けする
mail($sendto, $subject, $message, $headers, "-ffightingradialist@tri-international.org"); 	
//　最後の-f optionがenvelopeの存在を示すようだ
	
/*
Content-ID: <img_id_000>
	// この部分の空白行が必要　これが無いとファイルが壊れる
$img_encode64_001
*/
//	if ($count>1) exit;
}	// foreach
?>


<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
	h2 {color:red;}
</style>
<title>2nd Shonan TRI Maniac Conference</title>
</head>
<body bgcolor="#AADDFF">
<div align="center">
<h1>2nd Shonan TRI Maniac Conference<br/>
3種類の文書を　<?= $count ?>件送付しました<br /><br /></h1>

</div>

</body>
</html>

